import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rockets',
  templateUrl: './rockets.component.html',
  styleUrls: ['./rockets.component.scss']
})
export class RocketsComponent implements OnInit {
  public rockets: any[];
  @Output() getRocketsEvent = new EventEmitter<string>();

  constructor() {
    this.rockets = [];
  }

  ngOnInit(): void {
  }

  public getRockets() {
    console.log('trigger fetchRockets from rockets.component');
    this.getRocketsEvent.emit();
  }
}
