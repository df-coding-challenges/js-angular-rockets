import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-rocket-details',
  templateUrl: './rocket-details.component.html',
  styleUrls: ['./rocket-details.component.scss'],
})
export class RocketDetailsComponent implements OnInit {
  @Input() public foo: any;
  @Output() scheduleLaunchEvent = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  public scheduleLaunch(foo: any) {
    console.log('trigger fetchRockets from rockets.component');
    this.scheduleLaunchEvent.emit(foo);
  }
}
