import { Component, OnInit } from '@angular/core';
import { RocketsService } from 'src/app/services/rockets.service';

@Component({
  selector: 'app-space-center',
  templateUrl: './space-center.component.html',
  styleUrls: ['./space-center.component.scss'],
})
export class SpaceCenterComponent implements OnInit {
  public rockets: any[];
  public launches: any[];

  constructor(private rocketsService: RocketsService) {
    this.rockets = [];
    this.launches = [];
  }

  ngOnInit(): void {
  }

  public async getRockets(): Promise<any> {
    // TODO: Get data from rocketsService
  }

  public scheduleLaunch(rocket: any) {
    if (!this.launches.includes(rocket)) {
      this.launches.push(rocket);
    }
  }
}
