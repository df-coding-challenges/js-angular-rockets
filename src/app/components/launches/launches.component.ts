import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-launches',
  templateUrl: './launches.component.html',
  styleUrls: ['./launches.component.scss'],
})
export class LaunchesComponent implements OnInit {
  public launches: any[];

  constructor() {
    this.launches = [];
  }

  ngOnInit(): void {}

  public alertLaunch(launch: any) {
    alert(`Todo: remove rocket ${launch.id} from launch`);
  }
}
