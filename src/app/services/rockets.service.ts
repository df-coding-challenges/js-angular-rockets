import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, tap } from 'rxjs';
import { AppErrorHandler } from '../app.errors';

@Injectable({
  providedIn: 'root',
})
export class RocketsService {
  private readonly BASE_URL = 'https://api.spacexdata.com/v4';

  constructor(private readonly http: HttpClient) {}

  public fetchRockets(): Observable<any> {
    return this.http.get<any[]>(`${this.BASE_URL}/rockets`).pipe(
      tap((_) => _),
      catchError(AppErrorHandler.handleError<any[]>('fetchRockets', []))
    );
  }
}
