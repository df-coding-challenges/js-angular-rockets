import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SpaceCenterComponent } from './components/space-center/space-center.component';
import { RocketsComponent } from './components/rockets/rockets.component';
import { RocketDetailsComponent } from './components/rocket-details/rocket-details.component';
import { LaunchesComponent } from './components/launches/launches.component';
import { RocketsService } from './services/rockets.service';

@NgModule({
  declarations: [
    AppComponent,
    SpaceCenterComponent,
    RocketsComponent,
    RocketDetailsComponent,
    LaunchesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [RocketsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
